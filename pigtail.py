import cv2
import numpy as np
import pathlib
import segmentation as seg
import new_segment


debug = True
seg_debug = False


def debug_show(window, image):

    if debug:
        cv2.imshow(window, image)
        cv2.waitKey()
        cv2.destroyAllWindows()


def main():

    # loop through the images in the images directory
    cwd = pathlib.Path.cwd()
    directory = cwd / 'Classified_segments-tails_to_cut'

    for filename in sorted(directory.iterdir()):
        # check if the file ends with .jpg, ignore all others

        if filename.suffix == ".jpg": # and filename.name == "0_20190927230352290_192.168.123.110.jpg_segment.jpg":
            # convert path object to string and open the image
            image_name = str(filename)
            blob = cv2.imread(image_name, 1)
            #print("beginning the segmentation process")
            #try:
            #    success, segment = seg.segment_pig(blob, seg_debug)
            #except:
            #    continue

            #print("segmentation process completed")
            #debug_show("segmentation", segment)
            gray_blob = cv2.cvtColor(blob, cv2.COLOR_BGR2GRAY)

            # threshold the image to remove the jpeg compresion artifacts
            # once incorporated into the final project this step might not be needed.
            pig_blob = cv2.threshold(gray_blob, 120, 255, cv2.THRESH_BINARY)[1]

            # use the new function for just removing the head of the pig blob
            remove_limit = [10, 7000]
            remove_ratio = 0.3
            remove_threshold = 0.75
            headless_pig = new_segment.remove_head(pig_blob, remove_limit, remove_ratio, remove_threshold)

            """
            contours, hierarchy = cv2.findContours(headless_pig, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

            debug_show("Binary Image", headless_pig)

            # the minAreaRect will give us the area of the image we want to work with.
            rect = cv2.minAreaRect(contours[0])
            (x, y), (rect_width, rect_height), angle = rect
            if rect_height < rect_width:
                temp = rect_height
                rect_height = rect_width
                rect_width = temp

            # we must adjust for the angle that opencv draws at
            if angle < -45:
                correction_angle = angle + 90
                rot_angle = -(angle + 90)
            else:
                correction_angle = angle
                rot_angle = -angle

            correction_matrix = cv2.getRotationMatrix2D((x, y), correction_angle, 1)
            rot_matrix = cv2.getRotationMatrix2D((x, y), rot_angle, 1)
            image_height, image_width = pig_blob.shape[:2]

             TODO: divide image into three sections along the longest length. These should correspond to the head
            the body, and the back-end. Take a mask that is 1/3 that length, but the full length of the
            shorter length, and create three separate images with only the one section as a binary image.
            take contours of each section and find the centerpoint in the main image of that blob. Then
            draw a curve that connects all three points to approximate the spinal curve. At the tail end
            of the spinal curve, draw a cross to divide that blob into 4 quandrants. We are only interested
            in quadrants 1 and 4, the other two can be sectioned out of the image or ignored. 

            # section distance will be used to draw two lines the same distance from the center point to cut
            # the pig blob into 3 distinct blobs.
            section_width = rect_height/6
            section_height = rect_width/2
            x1, x2 = int(x-section_width), int(x+section_width)
            y1, y2 = int(y-section_height), int(y+section_height)

            new_image = headless_pig.copy()

            rotated_pig = cv2.warpAffine(new_image, correction_matrix, (image_width, image_height), cv2.INTER_NEAREST, +
                                         cv2.BORDER_REPLICATE)
            cv2.line(rotated_pig, (x1, y1), (x1, y2), (0, 0, 0), 8)
            cv2.line(rotated_pig, (x2, y1), (x2, y2), (0, 0, 0), 8)

            #debug_show("rotated pig", rotated_pig)

            piggyback = cv2.warpAffine(rotated_pig, rot_matrix, (image_width, image_height), cv2.INTER_NEAREST, +
                                       cv2.BORDER_REPLICATE)

            #debug_show("divided blobs", piggyback)

            divided_contours,hierarchy = cv2.findContours(piggyback, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            canvas = piggyback.copy()
            x_points = list()
            y_points = list()

            for contour in divided_contours:
                moments = cv2.moments(contour)
                if "m00" != 0:
                    center_x = int(moments["m10"] / moments["m00"])
                    center_y = int(moments["m01"] / moments["m00"])
                    x_points.append(center_x)
                    y_points.append(center_y)
                    cv2.circle(canvas, (center_x, center_y), 7, (0, 0, 0), -1)

            debug_show("center points", canvas)

            x_points, y_points = zip(*sorted(zip(x_points, y_points)))
            equation_vector = np.polyfit(x_points, y_points, 2)

            x_points2 = np.linspace(min(x_points), max(x_points), 200)
            y_points2 = np.polyval(equation_vector, x_points2)

            draw_points = (np.asarray([x_points2, y_points2]).T).astype(np.int32)

            cv2.polylines(canvas, [draw_points], False, (0, 0, 0), 2)

            debug_show("Spine", canvas)

            derivative = np.polyder(equation_vector)
            end_x = max(x_points)

            end_y = np.polyval(equation_vector, end_x)
            slope = np.polyval(derivative, end_x)
            end_angle = np.arctan(slope)
            print(end_angle)

            end_rotation_matrix = cv2.getRotationMatrix2D((end_x, end_y), end_angle, 1)
            """
            piggyback = new_segment.remove_tail(headless_pig)

            debug_show("piggyback", piggyback)



if __name__ == "__main__":
    main()