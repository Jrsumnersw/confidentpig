import cv2 as cv
import numpy as np
import pathlib
import openpyxl

def main():

    # loop through the images in the images directory
    cwd = pathlib.Path.cwd()
    directory = cwd / 'images'

    columns = ["Image#", "Confidence%", "Centerpoint", "Aspect Ratio", "Leakage", "Missing", "height", "width"]

    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = "Good segmentation images"
    i = 1
    for title in columns:
        c1 = sheet.cell(row=1, column=i)
        c1.value = title
        i += 1

    j = 2
    for filename in sorted(directory.iterdir()):
        # check if the file ends with .jpg, ignore all others

        if filename.suffix ==".jpg": #and filename.name == "0_20190927051351276_192.168.123.107.jpg_blob.jpg":
            # convert path object to string and open the image
            imagename = str(filename)
            blob = cv.imread(imagename, 1)
            newrow =[]
            # convert to greyscale
            grayblob = cv.cvtColor(blob, cv.COLOR_BGR2GRAY)

            # threshold the image to remove the jpeg compresion artifacts
            # once incorporated into the final project this step might not be needed.
            pig_blob = cv.threshold(grayblob, 120, 255, cv.THRESH_BINARY)[1]



            # find the image contours, many will be returned despite that there should only be 1
            contours, hierarchy = cv.findContours(pig_blob, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

            # the minAreaRect will give us the points that we will then convert into a contour
            # so that we can drawContours the rectangle onto the image for reference
            rect = cv.minAreaRect(contours[0])

            (x, y), (cwidth, cheight), angle = rect
            #print("rectangle width: %s" % str(cwidth))
            #print("rectangle height: %s" % str(cheight))
            if cheight < cwidth:
                temp = cheight
                cheight = cwidth
                cwidth = temp

            # find the exact centerpoint of the image
            nheight, nwidth = pig_blob.shape[:2]
            iCenterX = int(nwidth//2)
            iCenterY  = int(nheight//2)

            # find the divergence from the centerpoint by getting the absolute value
            # of (the centerpoint of the image - the centerpoint of the bounding box
            # the area represented by the deviation will be a percentage of the overall
            # image size and that will affect the confidence value by that percentage
            xDivergence = np.absolute(iCenterX - x)
            yDivergence = np.absolute(iCenterY - y)
            #totalDivergence = xDivergence*yDivergence
            #totalDivergence /= (nheight*nwidth*10)
            x_offset = xDivergence/iCenterX
            y_offset = yDivergence/iCenterY
            totalDivergence = (x_offset + y_offset)/10
            print("Total divergence from centerpoint: {:.2f} %" .format(totalDivergence))

            # use the Aspect ratio to reduce the confidence by the ratio of the bounding
            # rectangle ratio by the main image ratio
            if nheight > nwidth:
                imageratio = nheight/nwidth
            else:
                imageratio = nwidth/nheight
            aspectratio = cheight/cwidth
            aspectratio /= imageratio
            aspectratio = np.abs(1-aspectratio)
            print("Divergence of aspect ratio: {:.2f} %" .format(aspectratio))

            # we must adjust for the angle that opencv draws at
            if angle < -45:
                rotangle = -(angle + 90)
            else:
                rotangle = -(angle)

            # use the centerpoint, height, width, and angle to create the ideal pig
            # template in a new window that will then be used as a mask to get the
            # new contour and area to affect the confidence value
            radius = cwidth // 2

            difference = (cheight // 2) - (cwidth // 2)

            vertex1 = (int(x + (cheight / 2)), int(y + radius))
            vertex2 = (int(x - (cheight / 2)), int(y - radius))
            x1 = (x - difference)
            x2 = (x + difference)

            rotMatrix = cv.getRotationMatrix2D((x, y), rotangle, 1)
            box = cv.boxPoints(rect)
            box = np.intp(box)

            # create a brand new image the same size as the original image
            newimage = np.zeros_like(pig_blob)

            # now take the data calculated above to draw the circles at points
            # determined by the data passed into the method
            cv.circle(newimage, (int(x1), int(y)), int(radius), (255, 255, 255), -1)
            cv.circle(newimage, (int(x2), int(y)), int(radius), (255, 255, 255), -1)

            # and then draw the ellipsis with axis determined from the data as well
            cv.ellipse(newimage, (int(x), int(y)), (int(radius), int(difference)), -90, 0, 360, (255, 255, 255), -1, 0)

            # use the rotation matrix passed in so that we can transform and skew the image to match
            # the base image.
            rotated = cv.warpAffine(newimage, rotMatrix, (nwidth, nheight), cv.INTER_NEAREST, cv.BORDER_REPLICATE)
            newthresh = cv.threshold(rotated, 120, 255, cv.THRESH_BINARY)[1]
            newinversion = cv.bitwise_not(newthresh)

            # get the contour of the mask so we can get the contour area to divide the missing percent by
            maskarea = np.sum(newthresh)

            masked = cv.bitwise_and(pig_blob, newthresh)
            leaked = cv.bitwise_and(pig_blob, newinversion)

            leakage = np.sum(leaked)
            leakage /= maskarea
            print("percentage of pixels leaked: {:.2f} %" .format(leakage))
            missing = np.sum(np.subtract(newthresh, masked))
            missing /= maskarea
            print("percentage of pixels missing from the ideal: {:.2f} %" .format(missing))
            finalconfidence = 1 - (leakage + missing + aspectratio + totalDivergence)
            if finalconfidence < 0:
                finalconfidence = 0
            print("Final confidence value of the image: {:.2f} %" .format(finalconfidence))

            parts = filename.parts
            for part in parts:
                if part.endswith(".jpg"):
                   imagefilename = part

            newrow.append(imagefilename)
            newrow.append(finalconfidence)
            newrow.append(totalDivergence)
            newrow.append(aspectratio)
            newrow.append(leakage)
            newrow.append(missing)
            newrow.append(nheight)
            newrow.append(nwidth)

            index = 1
            for item in newrow:
                cx = sheet.cell(row=j, column=index)
                cx.value = item
                index += 1
            j += 1

            wb.save("data.xlsx")

            # show the old image
            #cv.imshow(imagename, pig_blob)
            # show the new image
            #cv.imshow("newimage", newthresh)
            #show the inverted image
            #cv.imshow("inversion", newinversion)

            #cv.waitKey(0)

            #cv.imshow("masked", masked)
            #cv.imshow("leaked", leaked)
            #cv.waitKey(0)
            #cv.destroyAllWindows()

            #exit()



if __name__ == "__main__":
    main()