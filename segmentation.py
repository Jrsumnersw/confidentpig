import cv2
import numpy as np
import vision as v
import contours
from typing import Tuple, Union


def debug_show(window, image, debug):

    if debug:
        cv2.imshow(window, image)
        cv2.waitKey()
        cv2.destroyAllWindows()


def segment_pig(image: np.ndarray, debug: bool = True) -> Tuple[bool, Union[None, np.ndarray], Union[None, np.ndarray]]:
    success = False
    segment = None
    pig_blob = None
    window = "Test Segmentation"

    # make it grey
    img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    debug_show(window, img, debug)

    img = v.median_blur(img)
    debug_show(window, img, debug)

    img = v.contrast(img, 2.5, -191)
    debug_show(window, img, debug)

    img = v.canny_otsu(img, ratio=0.2, gradientMode=True)
    debug_show(window, img, debug)

    _, img_contours = contours.contours(img)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.split_sharp_contours(img, img_contours, max_angle=40)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    # @Improvement potentially try to get a line of best fit through short clusters of lines/points
    img_contours = contours.filter_short_contours(img, img_contours, min_length=25)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.filter_touching_padding(img, img_contours, padding=(20, 20), debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.filter_touching_inner(img, img_contours, padding=(30, 15), percent=True, keep_contours_length=50, debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.merge_similar_contours(img, img_contours, min_intersections=20, debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.extend_contours(img, img_contours, extension_length=50, degree=1, point_lookahead=20, debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.clean_and_connect_contours(img, img_contours, max_angle=40, points_to_consider=125, debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.approximate_contours(img, img_contours, epsilon=0.02, debug_draw=False)
    debug_show(window, contours.draw_contours(img, img_contours), debug)

    img_contours = contours.filter_short_contours(img, img_contours, min_length=15)

    img, _ = contours.contours(img, initial_contours=img_contours, contour_thickness=1, debug_draw=False)
    debug_show(window, img, debug)

    img = v.dilate_floodfill_erode(img, max_tries=10, padding=30, max_overlap_percent=0.5, debug_draw=False)
    debug_show(window, img, debug)

    img, _ = contours.contours(img, fill=True, debug_draw=False)
    debug_show(window, img, debug)

    # Confidence; if no white pixels in this image are present, it's a complete fail
    if cv2.countNonZero(img) == 0:
        #logger.error("Failed to create a pig body segment")
        print("Failed to create a pig body segment")
        return success, segment, pig_blob

    segment = img

    # @TODO Select the biggest blob in the image

    mask = img.copy()

    # @TODO Read from config.ini file...
    remove_limit = [10, 7000]
    remove_ratio = 0.3
    remove_threshold = 0.75

    #pig_blob = pv.remove_head_tail(mask, remove_limit, remove_ratio, remove_threshold, debug_draw=debug_draw)
    success = True

    return success, segment