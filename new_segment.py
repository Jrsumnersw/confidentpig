import cv2
import numpy as np
import vision
from operator import itemgetter
from typing import List


def debug_show(image, debug_mode):

    if debug_mode:
        cv2.imshow("debug", image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def get_rightmost_contour(contours):
    last_x = 0
    for contour in contours:

        moments = cv2.moments(contour)
        if "m00" != 0:
            center_x = int(moments["m10"] / moments["m00"])

            if center_x > last_x:
                last_x = center_x
                final_contour = contour

    return final_contour


def filter_points(defect_points, defect_range, range_length, image_width):
    new_list = list()
    for point in defect_points:
        x, y = point
        point_index = range_length - (image_width-x)
        y_min, y_max = defect_range[point_index]

        if y_max > y > y_min:
            new_list.append(point)

    return new_list


def calculate_range(angle, x, y, length):
    top_angle = angle - 50
    theta = (top_angle*np.pi)/180
    bottom_angle = angle + 50
    gamma = (bottom_angle*np.pi)/180
    defect_range = {x: (y, y)}
    for i in range(length):
        y_min = y + (i*np.sin(theta))
        y_max = y + (i*np.sin(gamma))
        defect_range[i] = (y_min, y_max)

    return defect_range


def remove_tail(center: np.ndarray, debug_draw=False) -> np.ndarray:
    """

    :param center : ndarray: Input image.
    :param debug_draw : bool: Toggles whether to show the middle steps in a visual window.

    :returns final_image : ndarray: a binary image with the head section removed from the image
    """

    work_image = center.copy()
    contours, hierarchy = cv2.findContours(work_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    rect = cv2.minAreaRect(contours[0])
    (x, y), (rect_width, rect_height), angle = rect
    if rect_height < rect_width:
        temp = rect_height
        rect_height = rect_width
        rect_width = temp

    if angle < -45:
        correction_angle = angle + 90
        rot_angle = -(angle + 90)
    else:
        correction_angle = angle
        rot_angle = -angle

    correction_matrix = cv2.getRotationMatrix2D((x, y), correction_angle, 1)
    rot_matrix = cv2.getRotationMatrix2D((x, y), rot_angle, 1)
    image_height, image_width = work_image.shape[:2]

    section_width = rect_height / 6
    section_height = rect_width / 2
    x1, x2 = int(x - section_width), int(x + section_width)
    y1, y2 = int(y - section_height), int(y + section_height)

    rotated_pig = cv2.warpAffine(work_image, correction_matrix, (image_width, image_height), cv2.INTER_NEAREST, +
                                 cv2.BORDER_REPLICATE)

    cv2.line(rotated_pig, (x1, y1), (x1, y2), (0, 0, 0), 8)
    cv2.line(rotated_pig, (x2, y1), (x2, y2), (0, 0, 0), 8)

    debug_show(rotated_pig, debug_draw)
    piggyback = cv2.warpAffine(rotated_pig, rot_matrix, (image_width, image_height), cv2.INTER_NEAREST, +
                               cv2.BORDER_REPLICATE)
    debug_show(piggyback, debug_draw)

    divided_contours, hierarchy = cv2.findContours(piggyback, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    canvas = piggyback.copy()
    x_points = list()
    y_points = list()

    for contour in divided_contours:
        moments = cv2.moments(contour)
        if "m00" != 0:
            center_x = int(moments["m10"] / moments["m00"])
            center_y = int(moments["m01"] / moments["m00"])
            x_points.append(center_x)
            y_points.append(center_y)
            #cv2.circle(canvas, (center_x, center_y), 7, (0, 0, 0), -1)

    debug_show(canvas, debug_draw)

    x_points, y_points = zip(*sorted(zip(x_points, y_points)))
    equation_vector = np.polyfit(x_points, y_points, 2)

    x_points2 = np.linspace(min(x_points), max(x_points), 200)
    y_points2 = np.polyval(equation_vector, x_points2)

    draw_points = (np.asarray([x_points2, y_points2]).T).astype(np.int32)

    cv2.polylines(canvas, [draw_points], False, (0, 0, 0), 2)

    debug_show(canvas, debug_draw)
    derivative = np.polyder(equation_vector)
    end_x = max(x_points)

    end_y = int(np.polyval(equation_vector, end_x))
    slope = np.polyval(derivative, end_x)
    end_angle = np.arctan(slope)
    end_angle = end_angle * 180/3.14
    perpendicular_angle = end_angle + 90

    top_x = end_x - int(round(image_height * np.cos(perpendicular_angle * 3.14/180)))
    top_y = end_y - int(round(image_height * np.sin(perpendicular_angle * 3.14/180)))

    bottom_x = end_x + int(round(image_height * np.cos(perpendicular_angle * 3.14/180)))
    bottom_y = end_y + int(round(image_height * np.sin(perpendicular_angle * 3.14/180)))

    cv2.line(canvas, (end_x, end_y), (top_x, top_y), (0, 0, 0), 4)
    cv2.line(canvas, (end_x, end_y), (bottom_x, bottom_y), (0, 0, 0), 4)

    new_contours, hierarchy = cv2.findContours(canvas, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    last_contour = get_rightmost_contour(new_contours)

    right_mask = np.zeros_like(center)
    right_mask = cv2.drawContours(right_mask, [last_contour], 0, (255, 255, 255), -1)
    debug_show(right_mask, debug_draw)

    hull = cv2.convexHull(last_contour, returnPoints=False)
    contour_hull = cv2.convexHull(last_contour, returnPoints=True)
    defects = cv2.convexityDefects(last_contour, hull)

    test_mask = np.zeros_like(center)
    test_mask = cv2.drawContours(test_mask, [contour_hull], 0, (255),-1)
    debug_show(test_mask, debug_draw)

    defect_points = []
    for i in range(defects.shape[0]):
        s, e, f, d = defects[i, 0]
        depth = d/256.0

        if depth > 4:
            far_point = tuple(last_contour[f][0])
            #print(far_point)
            cv2.circle(test_mask, far_point, 5, (0), -1)
            defect_points.append(far_point)

    debug_show(test_mask, debug_draw)

    range_length = image_width - end_x
    defect_range = calculate_range(end_angle, end_x, end_y, range_length)
    defect_points = filter_points(defect_points, defect_range, range_length, image_width)

    if len(defect_points) > 1:
        defect_points.sort(key=itemgetter(1))
        last_index = len(defect_points)-1
        cv2.line(right_mask, defect_points[0], defect_points[last_index], (0), 4)

    debug_show(right_mask, debug_draw)

    left_mask = cv2.subtract(center, right_mask)
    debug_show(left_mask, debug_draw)

    if len(defect_points) > 1:
        junk_contours, hierarchy = cv2.findContours(right_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        junk = get_rightmost_contour(junk_contours)
        right_mask = cv2.drawContours(right_mask, [junk], 0, (0), -1)
    debug_show(right_mask, debug_draw)

    final_image = np.bitwise_or(left_mask, right_mask)
    debug_show(final_image, debug_draw)

    return final_image


def remove_head(center: np.ndarray, limit: List[int], ratio:float, threshold:float, debug_draw=False) -> np.ndarray:
    """Remove the head from binary image of a pig
    :param center : ndarray: Input image.
    :param limit : list: filter radius by given limit
        list -> [lower limit, upper limit]
    :param ratio : float: Ratio to determine how much overlap between circles there can be.
    :param threshold : float: Threshold is the ratio that is multiplied with the equivalent ellipse semi-minor axis.
        This is done to get a lower limit of the radius
    :param debug_draw : bool: Toggles whether to show the middle steps in a visual window.

    :returns center : ndarray: a binary image with the head section removed from the image
    """

    mask = np.zeros(center.shape, np.uint8)
    element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    circles_data = vision.find_circles_data(center, limit, ratio)
    contours, hierarchy = cv2.findContours(center, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    M = cv2.moments(contours[0])
    if M["m00"] != 0:
        c_X = int(M["m10"] / M["m00"])
        c_Y = int(M["m01"] / M["m00"])
    else:
        c_X, c_Y = 0, 0

    perimeter = cv2.arcLength(contours[0], True)
    area_segment = cv2.contourArea(contours[0])
    e1 = perimeter**2 / (2 * np.pi**2) + (2 * area_segment/np.pi)
    e2 = perimeter**2 / (2 * np.pi**2) - (2 * area_segment/np.pi)

    # minor axis of the equivalent ellipse
    E2b = np.sqrt(e1) - np.sqrt(e2)

    # semi-minor axis of the equivalent ellipse
    b = E2b/2

    filtered_circles = [item for item in circles_data if item[2] >= (b*threshold)]
    #min_index = circles_data.index(min(filtered_circles, key=itemgetter(0)))
    min_index = filtered_circles.index(min(filtered_circles, key=itemgetter(0)))

    x, y, r = filtered_circles[min_index]

    cv2.circle(mask, (x, y), round(r), 255, -1)

    diff = cv2.subtract(center, mask)
    debug_show(diff, debug_draw)
    diff = vision.morphology_open(diff, element, iterations=3, mode=True)
    debug_show(diff, debug_draw)
    diff = vision.remove_particles(diff, element, connectivity=8, iterations=10)
    debug_show(diff, debug_draw)

    retval, labels = cv2.connectedComponents(diff, connectivity=4, ltype=cv2.CV_16U)

    contours, hierarchy = cv2.findContours(diff, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    min_dist = 10000

    for contour in contours:

        M = cv2.moments(contour)
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        else:
            cX, cY = 0, 0

        dist = np.sqrt((cX-c_X)**2 + (cY-c_Y)**2)
        if min_dist > dist:
            min_dist = dist
            x = cX
            y = cY

    label = labels[y, x]

    diff[labels != label] = 0

    center = cv2.bitwise_or(mask, diff)
    debug_show(center, debug_draw)

    center = cv2.dilate(center, element, iterations=1)
    debug_show(center, debug_draw)

    return center
