import cv2
import numpy as np
import logging
from enum import IntEnum
from contextlib import suppress
from operator import itemgetter
from typing import List

logger = logging.getLogger("pigscale.helpers.vision")


def canny_mean(img: np.ndarray, ratioL: float = 0.66, ratioH: float = 1.33, sobel_size: int = 3, gradientMode: bool = False) -> np.ndarray:
    """Canny edges detection with threshold depending on the mean pixel value.

    Parameters
    ----------
    img :  ndarray
        Input image.
    ratioL : float
        Lower threshold value
    ratioH : float
        Higer threshold value
    sobel_size : int
        Aperture size for the Sobel operator.
    gradientMode : bool
        A flag, indicating whether a more accurate L2 norm will be used.

    Returns
    -------
    ndarray
        Return image with the edges.

    """

    mean_pixel = img.mean()
    return cv2.Canny(img, mean_pixel * ratioL, mean_pixel * ratioH, sobel_size, L2gradient=gradientMode)


def canny_median(img: np.ndarray, ratioL: float = 0.66, ratioH: float = 1.33, sobel_size: int = 3, gradientMode: bool = False) -> np.ndarray:
    """Canny edges detection with threshold depending on the median pixel value.

    Parameters
    ----------
    img :  ndarray
        Input image.
    ratioL : float
        Lower threshold value
    ratioH : float
        Higer threshold value
    sobel_size : int
        Aperture size for the Sobel operator.
    gradientMode : bool
        A flag, indicating whether a more accurate L2 norm will be used.

    Returns
    -------
    ndarray
        Return image with the edges.
    """

    median_pixel = np.median(img)
    return cv2.Canny(img, median_pixel * ratioL, median_pixel * ratioH, sobel_size, L2gradient=gradientMode)


def canny_otsu(img: np.ndarray, ratio: float = 0.5, sobel_size: int = 3, gradientMode: bool = False) -> np.ndarray:
    """Canny edges detection with threshold depending  Otsu's thresholding.

    Parameters
    ----------
    img : ndarray
        Input image.
    ratio : float
    Effect the lower threshold value
    sobel_size : int
        Aperture size for the Sobel operator.
    gradientMode : bool
        A flag, indicating whether a more accurate L2 norm will be used.

    Returns
    -------
    ndarray
        Return image with the edges.
    """

    otsu_thresh_val = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[0]
    return cv2.Canny(img, otsu_thresh_val * ratio, otsu_thresh_val, sobel_size, L2gradient=gradientMode)


def morphology_close(img: np.ndarray, element: np.ndarray, iterations: int = 1, mode: bool = True) -> np.ndarray:
    """Do a morphology close operations

    Parameters
    ----------
    img : ndarray
        Input image.
    element : ndarray
        Structuring element.
    iterations : int
        Number of iterations.
    mode : bool
        A flag, indicating whether to run all dilation operations first
        then all erosion operations or run dilation operations first
        then after run erosion operations iterative

    Returns
    -------
    ndarray
        Return a image.

    """

    if mode:
        img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, element, iterations=iterations)

    else:
        for i in range(iterations):
            img = cv2.dilate(img, element, iterations=1)
            img = cv2.erode(img, element, iterations=1)

    return img


def morphology_open(img: np.ndarray, element: np.ndarray, iterations: int = 1, mode: bool = True) -> np.ndarray:
    """Do a morphology open operations

    Parameters
    ----------
    img : ndarray
        Input image.
    element : ndarray
        Structuring element.
    iterations : int
        Number of iterations.
    mode : bool
        A flag, indicating whether to run all erosion operations first
        then all dilation operations or run erosion operations first
        then after run dilation operations iterative

    Returns
    -------
    ndarray
        Return a image.
    """

    if mode:
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, element, iterations=iterations)

    else:
        for i in range(iterations):
            img = cv2.erode(img, element, iterations=1)
            img = cv2.dilate(img, element, iterations=1)

    return img


def remove_particles(img: np.ndarray, element: np.ndarray, connectivity: int = 4, iterations: int = 2) -> np.ndarray:
    """ Keeps particles resistant to a specified structuring element
    The particles that are kept are exactly the same shape as those found in the original source image

    Parameters
    ----------
    img : ndarray
        Input image.
    element : ndarray
        Structuring element.
    connectivity : int
        Number of neighbors.
    iterations : int
        Number of iterations.

    Returns
    -------
    ndarray
        Return a image.

    """

    mask = cv2.erode(img, element, iterations=iterations)
    binary_img = cv2.threshold(img, 1, 255, cv2.THRESH_BINARY)[1]
    binary_mask = cv2.threshold(mask, 1, 255, cv2.THRESH_BINARY)[1]
    amount, labels_img = cv2.connectedComponents(binary_img, connectivity=connectivity)

    mask = np.uint8(mask)
    values = np.unique(labels_img[binary_mask == 255])

    for i in values:
        mask[labels_img == i] = 1

    img[mask == 0] = 0
    return img


def compare_neighbors(arr: np.ndarray) -> np.ndarray:
    """Compare nearby 4 neighbors values in a matrix.
        If nearby neighbors is greater then the center point
        It set true a bool matrix in the same positions as the center point.
        If not value set False

    Parameters
    ----------
    arr : ndarray
        Input numpy matrix.

    Returns
    -------
    ndarray
        Return matrix of type bool.

    """

    comp_arr = np.full(arr.shape, False, dtype=bool)

    for (x, y), item in np.ndenumerate(arr):

        if x >= 0:
            if arr[x - 1, y] > item:
                comp_arr[x, y] = True
                continue

        with suppress(IndexError):
            if arr[x + 1, y] > item:
                comp_arr[x, y] = True
                continue

        with suppress(IndexError):
            if arr[x, y + 1] > item:
                comp_arr[x, y] = True
                continue

        if y >= 0:
            if arr[x, y - 1] > item:
                comp_arr[x, y] = True
                continue

    return comp_arr


def label_fill_holes(img: np.ndarray, connectivity: int = 4) -> np.ndarray:
    """ Filling holes in regions on a images.

    Parameters
    ----------
    img : ndarray
        Input image.
    connectivity : int
        Number of neighbors.

    Returns
    -------
    ndarray
        Return a images where holes is filled.

    """

    mask = np.zeros(img.shape, dtype=np.uint8)
    img_inv = cv2.bitwise_not(img)
    retval, labels = cv2.connectedComponents(img_inv, connectivity=connectivity, ltype=cv2.CV_16U)
    remove = labels.copy()
    remove[1:-1, 1:-1] = 0
    remove = np.unique(remove)

    for value in remove:
        if value > 0:
            labels[labels == value] = 0

    label_nbr = np.unique(labels)

    for nbr in label_nbr:
        if nbr > 0:
            mask[labels == nbr] = 255

    img = cv2.bitwise_or(img, mask)

    return img


def find_circles_data(img: np.ndarray, limit: List[int], ratio: float = 0.03) -> List[List[int]]:
    """ Try to fit circles in segments of given images.
    By using distance map to determine the radius of each segments.

    Parameters
    ----------
    img : ndarray
        Input image.
    limit : list
        filter radius by given limit
        list -> [lower limit, upper limit]
    ratio : float
        Ratio determinate how much overlap between circles can have.


    Returns
    -------
    list
        Return list with a list with center point of the circle
        and its radius => [x,y,r]

    """

    dist = cv2.distanceTransform(img, cv2.DIST_L2, 0)

    # TODO: test cv2.ximgproc.thinning if it is better than skeletonization method
    # mask1 = cv2.ximgproc.thinning(img, thinningType = cv2.ximgproc.THINNING_ZHANGSUEN)
    mask = skeletonization(img)

    dist[mask == 0] = 0

    dist[(dist < limit[0]) | (dist > limit[1])] = 0

    y, x = (dist > 0).nonzero()
    filtered_circle = []

    for i in range(len(y)):

        radius = int(round(dist[y[i], x[i]]))
        filtered_circle.append([x[i], y[i], radius])

    filtered_circle.sort(key=itemgetter(2), reverse=True)

    index = 0
    index2 = 1
    index_end = len(filtered_circle) - 1

    while True:

        x, y, r1 = filtered_circle[index]
        x2, y2, r2 = filtered_circle[index2]
        d = np.sqrt((x - x2)**2 + (y - y2)**2)

        if d <= abs(r1 - r2):

            del filtered_circle[index2]
            index_end -= 1

        elif d >= (r1 + r2):
            index2 += 1

        else:

            d1 = (r1**2 - r2**2 + d**2) / (2 * d)
            d2 = d - d1
            intersection_area = r1**2 * np.arccos(d1 / r1) - d1 * np.sqrt(r1**2 - d1**2) + r2**2 * np.arccos(d2 / r2) - d2 * np.sqrt(r2**2 - d2**2)
            area = np.pi * r1**2

            if intersection_area / area > ratio:
                del filtered_circle[index2]
                index_end -= 1

            else:
                index2 += 1

        if index2 >= index_end:
            index += 1
            index2 = index + 1

        if index >= index_end - 1 and index2 >= index_end:
            break

    return filtered_circle


def skeletonization(img: np.ndarray) -> np.ndarray:
    """Makes a skeletons of the particles within an image.

    Parameters
    ----------
    img : ndarray
        Input image.

    Returns
    -------
    ndarray
        Returns a images of skeletons.

    """

    skeleton = np.zeros(img.shape, np.uint8)

    element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    while True:
        erode = cv2.erode(img, element)
        mask = cv2.dilate(erode, element)
        mask = cv2.subtract(img, mask)
        skeleton = cv2.bitwise_or(skeleton, mask)
        img = erode.copy()

        if cv2.countNonZero(img) == 0:
            break

    return skeleton


def median_blur(img: np.ndarray) -> np.ndarray:
    return cv2.medianBlur(img, 9)


def contrast(img: np.ndarray, alpha: float = 1.5, beta: float = -5.) -> np.ndarray:
    new_image = np.zeros_like(img)
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            new_image[y, x] = np.clip(alpha * img[y, x] + beta, 0, 255)

    return new_image


class Direction(IntEnum):
    UP = 0
    LEFT = 1
    DOWN = 2
    RIGHT = 3

    @classmethod
    # This is here to make this easily wrap around
    def _missing_(cls, key):
        return Direction.UP


def dilate_floodfill_erode(img: np.ndarray, max_tries: int = 10, padding: int = 30, max_overlap_percent: float = 0.5, contour_thickness: int = 2, debug_draw: bool = False) -> np.ndarray:
    # The idea; run after the contour fixers
    # Dilate the contours and try filling a black pixel in the middle of the image.
    # If the area is too large, it probably didn't close correctly, so dilate and try filling again
    # When filled, erode so that the external lines disappear

    copy = img.copy()
    blank = np.zeros_like(img)
    kernel_shape = cv2.MORPH_ELLIPSE
    kernel_size = (9, 9)
    kernel = cv2.getStructuringElement(kernel_shape, kernel_size)

    leak_mask = np.pad(array=np.zeros((img.shape[0] - 2 * padding, img.shape[1] - 2 * padding), np.uint8),
                       pad_width=padding,
                       mode="constant",
                       constant_values=255)

    leak_mask_number_white = cv2.countNonZero(leak_mask)

    for i in range(max_tries):
        copy = cv2.dilate(copy, kernel, iterations=i)
        copy = cv2.erode(copy, kernel, iterations=i)
        logger.debug("dilate_floodfill_erode; try: %d", i)

        th, im_th = cv2.threshold(copy, 0, 255, cv2.THRESH_BINARY)

        im_floodfill = im_th.copy()

        h, w = im_th.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        # Find a start point that is black in the middle of the image
        seed_x = w // 2
        seed_y = h // 2
        seed_point = None

        target_distance = 1
        distance = 0
        direction = Direction.UP

        if debug_draw:
            seed = im_th.copy()
        while True:
            seed_point = (seed_x, seed_y)
            '''
            if debug_draw:
                seed = cv2.rectangle(seed, seed_point, seed_point, (127, 127, 127), -1)
                cv2.imshow("floodfill seed", seed)
                cv2.waitKey(1)
            '''
            if im_th[seed_y][seed_x] == 0:
                break

            if direction is Direction.LEFT:
                seed_x -= 1
            elif direction is Direction.RIGHT:
                seed_x += 1
            elif direction is Direction.UP:
                seed_y -= 1
            elif direction is Direction.DOWN:
                seed_y += 1

            distance += 1
            if distance == target_distance:
                distance = 0
                direction = Direction(direction + 1)
                if direction in [Direction.UP, Direction.DOWN]:
                    target_distance += 1

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", im_floodfill)
            cv2.waitKey(0)

        # Floodfill the interior
        cv2.floodFill(im_floodfill, mask, seed_point, 255)

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", im_floodfill)
            cv2.waitKey(0)

        # Extract just the filled blob
        im_floodfill = ~(im_th | ~im_floodfill)

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", im_floodfill)
            cv2.waitKey(0)

        # Dilate to fill parts missing internally
        im_floodfill = cv2.dilate(im_floodfill, kernel, iterations=contour_thickness)
        # Erode half the distance to reduce outer blob to size
        im_floodfill = cv2.erode(im_floodfill, kernel, iterations=contour_thickness // 2)

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", im_floodfill)
            cv2.waitKey(0)

        # Save any overlap with the leak border
        leak_test = im_floodfill & leak_mask

        # If there didn't leak any white pixels to the outside, it was successful
        if cv2.countNonZero(leak_test) / leak_mask_number_white <= max_overlap_percent / 100:
            success = True
        else:
            continue

        blank = img | im_floodfill

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", blank)
            cv2.waitKey(0)

        # Erode away the external contours
        blank = cv2.erode(blank, kernel, iterations=contour_thickness)
        # And dilate the main blob back again
        blank = cv2.dilate(blank, kernel, iterations=contour_thickness)

        if debug_draw:
            cv2.imshow("dilate_floodfill_erode", blank)
            cv2.waitKey(0)

        if success:
            break

    return blank
