import cv2
import numpy as np
import logging
import functools

logger = logging.getLogger("pigscale.helpers.contours")


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2, shortest=False):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)) * 180/np.pi
    # we want to ignore the direction of the vectors and only check the angle between the lines
    if shortest and angle > 90:
        angle = 90 - (angle - 90)
    return angle


def to_vector(a, b):
    if type(a) is not tuple:
        pa = to_point(a)
    else:
        pa = a
    if type(b) is not tuple:
        pb = to_point(b)
    else:
        pb = b
    return pb[0] - pa[0], pb[1] - pa[1]


def to_point(point):
    return point[0][0], point[0][1]


def draw_contours(img, contours, debug_draw=False):
    line_color = (255, 255, 255)
    start_color = (0, 255, 0)
    end_color = (0, 0, 255)
    # Create a blank of same size as img supporting color
    blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
    for contour in contours:
        prev_point = None
        for point in contour:
            point = (point[0][0], point[0][1])
            if prev_point is not None:
                cv2.line(blank, prev_point, point, line_color)
            else:
                cv2.drawMarker(blank, point, start_color, cv2.MARKER_TILTED_CROSS)
            prev_point = point
        cv2.drawMarker(blank, point, end_color, cv2.MARKER_TILTED_CROSS)

    if debug_draw:
        cv2.imshow("contours", blank)
        cv2.waitKey(0)

    return blank


def split_sharp_contours(img, contours, point_lookahead=3, max_angle=50):
    logger.debug(split_sharp_contours.__name__)

    new_contours = []
    for contour in contours:
        current_contour_start = 0
        i = 0
        while i < len(contour) - 2 * point_lookahead:
            prevs = contour[i:i+point_lookahead]
            avg_prev = functools.reduce((lambda x, y: x + y), prevs) / point_lookahead
            center = contour[i+point_lookahead]
            nexts = contour[i+1+point_lookahead:i+1+point_lookahead*2]
            avg_next = functools.reduce((lambda x, y: x + y), nexts) / point_lookahead

            CP = to_vector(avg_prev, center)
            CN = to_vector(center, avg_next)
            angle = angle_between(CP, CN)

            if angle > max_angle:
                # Make the split
                new_contour = np.array(contour[current_contour_start:i+point_lookahead])
                new_contours.append(new_contour)
                current_contour_start = i+point_lookahead
                i = current_contour_start
            else:
                i += 1

        # Add the last one
        new_contour = np.array(contour[current_contour_start:])
        new_contours.append(new_contour)

    return new_contours


def filter_short_contours(img, contours, min_length=50):
    logger.debug(filter_short_contours.__name__)

    # use manhattan distance between points
    new_contours = []
    for c in contours:
        manhattan_distance = 0
        for i in range(len(c) - 1):
            pt1 = c[i][0]
            pt2 = c[i+1][0]
            manhattan_distance += abs(pt2[0] - pt1[0]) + abs(pt2[1] - pt1[1])
            if manhattan_distance >= min_length:
                new_contours.append(c)
                break

    return new_contours


# @Improvement add some fluffyness to the touching check allowing contours to be touching a little bit
#  This in order to be able to expand the padding area to get rid of more garbage lines, without
#  blindly throwing out long valid ones
def filter_touching_padding(img, contours, padding=(30, 30), percent=False, debug_draw=False):
    logger.debug(filter_touching_padding.__name__)
    height, width = img.shape

    if percent:
        padding = (width * padding[0] // 100, height * padding[1] // 100)

    width_boundary = (padding[0], width - padding[0])
    height_boundary = (padding[1], height - padding[1])

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        blank = cv2.rectangle(blank, (width_boundary[0], height_boundary[0]), (width_boundary[1], height_boundary[1]), color=(0, 0, 255))
        cv2.imshow('touching padding filter', blank)
        cv2.waitKey(0)

    new_contours = []
    for contour in contours:
        keep = True
        for p in [to_point(x) for x in contour]:

            # If we're not inside the outer boundary, don't check further and don't keep this
            if not (width_boundary[0] < p[0] < width_boundary[1] and height_boundary[0] < p[1] < height_boundary[1]):
                keep = False
                break

        if debug_draw:
            blank = cv2.polylines(blank, [contour], False, (0, 255, 0), 1)
            ip = (int(p[0]), int(p[1]))
            if not keep:
                blank = cv2.drawMarker(blank, ip, (0, 255, 255))
            cv2.imshow('touching padding filter', blank)
            cv2.waitKey(0)

        if keep:
            new_contours.append(contour)

    return new_contours


# @Improvement add some fluffyness to the touching check allowing contours to be touching a little bit
#  This in order to be able to expand the padding area to get rid of more garbage lines, without
#  blindly throwing out long valid ones
def filter_touching_inner(img, contours, padding=(30, 10), percent=True, keep_contours_length=-1, debug_draw=False):
    logger.debug(filter_touching_inner.__name__)
    height, width = img.shape

    if percent:
        padding = (width * padding[0] // 100, height * padding[1] // 100)

    width_boundary = (width // 2 - padding[0], width // 2 + padding[0])
    height_boundary = (height // 2 - padding[1], height // 2 + padding[1])

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        blank = cv2.rectangle(blank, (width_boundary[0], height_boundary[0]), (width_boundary[1], height_boundary[1]), color=(0, 0, 255))
        cv2.imshow('touching padding filter', blank)
        cv2.waitKey(0)

    new_contours = []
    for contour in contours:
        keep = True
        for p in [to_point(x) for x in contour]:

            # If we are inside the inner boundary, don't check further and don't keep this
            if width_boundary[0] < p[0] < width_boundary[1] and height_boundary[0] < p[1] < height_boundary[1]:
                if keep_contours_length > -1 and len(contour) < keep_contours_length:
                    keep = False
                break

        if debug_draw:
            blank = cv2.polylines(blank, [contour], False, (0, 255, 0), 1)
            ip = (int(p[0]), int(p[1]))
            if not keep:
                blank = cv2.drawMarker(blank, ip, (0, 255, 255))
            cv2.imshow('touching padding filter', blank)
            cv2.waitKey(0)

        if keep:
            new_contours.append(contour)

    return new_contours


def bresenhams_line_algorithm(x0, y0, x1, y1):
    """
    Written based off of pseudocode from Wikipedia
    :param x0:
    :param y0:
    :param x1:
    :param y1:
    :return: List of points
    """

    def plot_line_low(x0, y0, x1, y1):
        ret = []
        dx = x1 - x0
        dy = y1 - y0
        yi = 1
        if dy < 0:
            yi = -1
            dy = -dy
        D = 2*dy - dx
        y = y0

        for x in range(x0, x1, 1 if x0 <= x1 else -1):
            ret.append((x, y))
            if D > 0:
                y = y + yi
                D = D - 2*dx
            D = D + 2*dy

        return ret

    def plot_line_high(x0, y0, x1, y1):
        ret = []
        dx = x1 - x0
        dy = y1 - y0
        xi = 1
        if dy < 0:
            xi = -1
            dx = -dx
        D = 2*dx - dy
        x = x0

        for y in range(y0, y1, 1 if y0 <= y1 else -1):
            ret.append((x, y))
            if D > 0:
                x = x + xi
                D = D - 2*dy
            D = D + 2*dx

        return ret

    if abs(y1 - y0) < abs(x1 - x0):
        if x0 > x1:
            return plot_line_low(x1, y1, x0, y0)
        else:
            return plot_line_low(x0, y0, x1, y1)
    else:
        if y0 > y1:
            return plot_line_high(x1, y1, x0, y0)
        else:
            return plot_line_high(x0, y0, x1, y1)


def extend_contours(img, contours, extension_length=20, point_lookahead=20, degree=2, debug_draw=False):
    # @TODO @Improvement Autodetect whether to use 1st or 2nd degree linear regression
    logger.debug(extend_contours.__name__)
    height, width = img.shape
    new_contours = []

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        cv2.imshow('extend contours', blank)
        cv2.waitKey(0)

    for contour in contours:
        if debug_draw:
            blank = cv2.polylines(blank, [contour], False, (0, 255, 0), 1)
            cv2.imshow('extend contours', blank)
            cv2.waitKey(0)

        # if the contour is too short, we can't extend it; just append it
        if len(contour) < 2 * point_lookahead:
            new_contours.append(contour)
            continue

        extended_start = []
        extended_end = []

        # start of path
        start = contour[:point_lookahead, 0]
        sx = start[:, 0]
        sy = start[:, 1]
        try:
            z = np.polyfit(sx, sy, degree)
            f = np.poly1d(z)

            increasing = sx[0] - sx[-1] < 0
            # @Correctness this should probably be a loop to generate
            # the number of points necessary instead basing on extension_length
            stop = sx[0] + (-extension_length if increasing else extension_length) * 2
            new_sx = np.linspace(sx[0], stop).astype(int)
            new_sy = f(new_sx).astype(int)
            new_points = list(zip(new_sx, new_sy))
            # remove consecutive duplicates
            new_points = functools.reduce(lambda x, y: x + [y] if not x or x[-1] != y else x, new_points, [])

            # Create actual pixel points from the point of the generated line to follow contour structure
            pixel_points = []
            for i in range(len(new_points) - 1):
                p1 = new_points[i]
                p2 = new_points[i+1]
                pixel_points.extend(bresenhams_line_algorithm(*p1, *p2))

            new_points = pixel_points

            # cut the line at any invalid points
            invalid_idx = next((i for i, p in enumerate(new_points) if not (0 <= p[0] < width and 0 <= p[1] < height)), None)
            if invalid_idx is not None:
                new_points = new_points[:invalid_idx]

            # only keep the points within the extension length distance
            new_points = new_points[:extension_length]

            # restructure to contours shape and reverse it as this is the start
            new_points = [[x] for x in new_points[::-1]]
            extended_start = np.array(new_points)

            if debug_draw:
                for p in range(len(extended_start) - 1):
                    blank = cv2.line(blank, new_points[p][0], new_points[p+1][0], (0, 255, 255), 1)
                    # blank = cv2.polylines(blank, [extended_start], False, (0, 255, 255), 1)
                    cv2.imshow('extend contours', blank)
                    cv2.waitKey(10)

        except np.linalg.LinAlgError as e:
            logger.error(e)

        # end of path
        end = contour[-point_lookahead:, 0]
        ex = end[:, 0]
        ey = end[:, 1]
        try:
            z = np.polyfit(ex, ey, degree)
            f = np.poly1d(z)

            increasing = ex[0] - ex[-1] < 0
            # @Correctness this should probably be a loop to generate
            # the number of points necessary instead basing on extension_length
            stop = ex[-1] + (-extension_length if not increasing else extension_length) * 2
            new_ex = np.linspace(ex[-1], stop).astype(int)
            new_ey = f(new_ex).astype(int)
            new_points = list(zip(new_ex, new_ey))
            # remove consecutive duplicates
            new_points = functools.reduce(lambda x, y: x + [y] if not x or x[-1] != y else x, new_points, [])

            # Create actual pixel points from the point of the generated line to follow contour structure
            pixel_points = []
            for i in range(len(new_points) - 1):
                p1 = new_points[i]
                p2 = new_points[i+1]
                pixel_points.extend(bresenhams_line_algorithm(*p1, *p2))

            new_points = pixel_points

            # cut the line at any invalid points
            invalid_idx = next((i for i, p in enumerate(new_points) if not (0 <= p[0] < width and 0 <= p[1] < height)), None)
            if invalid_idx is not None:
                new_points = new_points[:invalid_idx]

            # only keep the points within the extension length distance
            new_points = new_points[:extension_length]

            # restructure to contours shape
            new_points = [[x] for x in new_points]
            extended_end = np.array(new_points)

            if debug_draw:
                for p in range(len(extended_end) - 1):
                    blank = cv2.line(blank, new_points[p][0], new_points[p+1][0], (0, 255, 255), 1)
                    # blank = cv2.polylines(blank, [extended_start], False, (0, 255, 255), 1)
                    cv2.imshow('extend contours', blank)
                    cv2.waitKey(10)

        except np.linalg.LinAlgError as e:
            logger.error(e)

        new_contour = np.concatenate((extended_start, contour, extended_end))
        if debug_draw:
            for p in range(len(new_contour) - 1):
                blank = cv2.line(blank, to_point(new_contour[p]), to_point(new_contour[p+1]), (255, 255, 255), 1)
                cv2.imshow('extend contours', blank)
                cv2.waitKey(10)

        new_contours.append(new_contour)
        if debug_draw:
            cv2.waitKey(0)

    return new_contours


def approximate_contours(img, contours, epsilon=0.2, debug_draw=False):
    logger.debug(approximate_contours.__name__)
    new_contours = []

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        cv2.imshow('approximate contours', blank)
        cv2.waitKey(0)

    for contour in contours:
        if debug_draw:
            blank = cv2.polylines(blank, [contour], False, (0, 255, 0), 1)
            cv2.imshow('approximate contours', blank)
            cv2.waitKey(0)

        approx_contour = cv2.approxPolyDP(contour, epsilon, False)

        if debug_draw:
                blank = cv2.polylines(blank, [approx_contour], False, (0, 0, 255), 1)
                cv2.imshow('approximate contours', blank)
                cv2.waitKey(0)

        new_contours.append(approx_contour)

    return new_contours


def clean_and_connect_contours(img, contours, max_angle=30, points_to_consider=40, gradient_points=10, debug_draw=False):
    # @Note that this algorithm assumes the contours contain a point for each pixel and is NOT approximated
    logger.debug(clean_and_connect_contours.__name__)
    logger.debug("number of contours: %d", len(contours))

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        cv2.imshow('clean and connect contours', blank)
        cv2.waitKey(0)

    sorted_contours = sorted(contours, key=lambda x: len(x), reverse=True)

    # @Performance is theoretically crazy bad: O(N^4), I think. Though for the current use case it isn't terrible
    i = 0
    while i < len(sorted_contours):
        contour_connected = False
        contour = sorted_contours[i]

        # don't check any contours less than 2*points_to_consider
        # it will break the algorithm, as beginning and end will overlap
        # and it may throw away everything
        if len(contour) < 2*points_to_consider:
            break

        if debug_draw:
            blank = cv2.polylines(blank.copy(), [contour], False, (0, 255, 0), 1)
            cv2.imshow('clean and connect contours', blank)
            cv2.waitKey(0)

        # check up to half the contour for self intersection
        # quick check if self intersecting to save time
        black = np.zeros_like(img)
        quarter_length = len(contour) // 4
        self_intersect = cv2.polylines(black.copy(), [contour[:quarter_length]], False, (255, 255, 255), 2) & \
            cv2.polylines(black.copy(), [contour[-quarter_length:]], False, (255, 255, 255), 2)

        if cv2.countNonZero(self_intersect) > 0:
            for b_idx, b_point in enumerate(contour[:quarter_length]):
                for e_idx, e_point in reversed(list(enumerate(contour))[-quarter_length:]):
                    # Check the points and their 4-neighbours for intersection
                    b_points = [to_point(x) for x in [
                        b_point,  # Center
                        b_point + (0, -1),  # Top
                        b_point + (0, 1),  # Bottom
                        b_point + (-1, 0),  # Left
                        b_point + (1, 0),  # Right
                    ]]
                    e_points = [to_point(x) for x in [
                        e_point,  # Center
                        e_point + (0, -1),  # Top
                        e_point + (0, 1),  # Bottom
                        e_point + (-1, 0),  # Left
                        e_point + (1, 0),  # Right
                    ]]

                    # if there's no intersection, move on
                    if set(b_points).isdisjoint(e_points):
                        continue

                    if debug_draw:
                        # Draw main contour in blue
                        intersection = cv2.polylines(blank.copy(), [contour], False, (255, 0, 0), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, to_point(contour[-1]), (255, 0, 0), cv2.MARKER_DIAMOND)

                        intersection = cv2.drawMarker(intersection, b_points[0], (0, 255, 0), cv2.MARKER_TILTED_CROSS)
                        intersection = cv2.drawMarker(intersection, e_points[0], (0, 0, 255), cv2.MARKER_TILTED_CROSS)
                        cv2.imshow('clean and connect contours - intersection', intersection)
                        cv2.waitKey(0)

                    logger.debug("Self-intersection")
                    # trim the excess from the contour
                    new_contour = contour[b_idx:e_idx]

                    contour_connected = True
                    sorted_contours[i] = new_contour
                    break

                if contour_connected:
                    break

        if contour_connected:
            if debug_draw:
                for p in range(len(sorted_contours[i]) - 1):
                    pt1 = to_point(sorted_contours[i][p])
                    pt2 = to_point(sorted_contours[i][p+1])
                    blank = cv2.line(blank, pt1, pt2, (0, 127, 255), 1)
                    cv2.imshow('clean and connect contours', blank)
                    cv2.waitKey(1)
                if i < len(sorted_contours):
                    blank = cv2.polylines(blank, [sorted_contours[i]], False, (0, 255, 0), 1)
            # Since we connected this contour to itself, we don't want to check
            # for further extensions of it; move on to next contour
            i += 1
            continue

        # only care about intersections at the ends of the line, we don't want a split in the middle of a long contour
        c_beginning = contour[:points_to_consider]
        c_end = contour[-points_to_consider:]
        c_ends_test = cv2.polylines(black.copy(), [c_beginning, c_end], False, (255, 255, 255), 2)
        contour_ends = []
        contour_ends.extend(c_beginning)
        contour_ends.extend(c_end)

        j = i + 1
        while j < len(sorted_contours):
            next_contour = sorted_contours[j]
            # don't check any contours less than 2*points_to_consider
            # it will break the algorithm, as beginning and end will overlap
            # and it may throw away everything
            if len(next_contour) < 2*points_to_consider:
                break

            n_beginning = next_contour[:points_to_consider]
            n_end = next_contour[-points_to_consider:]

            # quickly check if there might be an intersection
            n_ends_test = cv2.polylines(black.copy(), [n_beginning, n_end], False, (255, 255, 255), 2)

            if cv2.countNonZero(c_ends_test & n_ends_test) == 0:
                j += 1
                continue

            next_contour_ends = []
            next_contour_ends.extend(n_beginning)
            next_contour_ends.extend(n_end)

            # check all points to find intersections
            for c_idx, c_point in enumerate(contour_ends):
                for n_idx, n_point in enumerate(next_contour_ends):
                    # Check the points and their 4-neighbours for intersection
                    c_points = [to_point(x) for x in [
                        c_point,  # Center
                        c_point + (0, -1),  # Top
                        c_point + (0, 1),  # Bottom
                        c_point + (-1, 0),  # Left
                        c_point + (1, 0),  # Right
                    ]]
                    n_points = [to_point(x) for x in [
                        n_point,  # Center
                        n_point + (0, -1),  # Top
                        n_point + (0, 1),  # Bottom
                        n_point + (-1, 0),  # Left
                        n_point + (1, 0),  # Right
                    ]]

                    # If there aren't any intersection between the points in the sets, move on
                    if set(c_points).isdisjoint(n_points):
                        continue

                    if debug_draw:
                        # Draw main contour in blue
                        intersection = cv2.polylines(blank.copy(), [contour], False, (255, 0, 0), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, to_point(contour[-1]), (255, 0, 0), cv2.MARKER_DIAMOND)
                        # Draw secondary line in red
                        intersection = cv2.polylines(intersection, [next_contour], False, (0, 0, 255), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, to_point(next_contour[-1]), (0, 0, 255), cv2.MARKER_DIAMOND)
                        # Draw the intersection in yellow
                        intersection = cv2.drawMarker(intersection, to_point(c_point), (0, 255, 255), cv2.MARKER_TILTED_CROSS)
                        cv2.imshow('clean and connect contours - intersection', intersection)
                        cv2.waitKey(0)

                    # find if the intersection points were at the beginning or end of the contours
                    c_intersect_beginning = c_idx < points_to_consider
                    n_intersect_beginning = n_idx < points_to_consider

                    # Now extract the actual indices of the intersecting point on the original contours
                    c_intesection_idx = c_idx if c_intersect_beginning else len(contour) - (2*points_to_consider - c_idx)
                    n_intesection_idx = n_idx if n_intersect_beginning else len(next_contour) - (2*points_to_consider - n_idx)

                    ## Calculate the gradient lines at the contours' intersections
                    p_intersect = to_point(c_point)

                    # Calculate the contour tangent vector at c_intersection_idx
                    c_g_before = contour[max(0, c_intesection_idx - gradient_points):c_intesection_idx]
                    c_g_after = contour[c_intesection_idx+1:c_intesection_idx+1+gradient_points]
                    c_g_points_considered = min(len(c_g_before), len(c_g_after))

                    if c_g_points_considered != 0:
                        c_g_before = c_g_before[-c_g_points_considered:]
                        c_g_after = c_g_after[:c_g_points_considered]

                        avg_c_g_before = functools.reduce((lambda x, y: x + y), c_g_before) / c_g_points_considered
                        avg_c_g_after = functools.reduce((lambda x, y: x + y), c_g_after) / c_g_points_considered

                        c_tangent_vec = to_point(unit_vector([np.array(to_vector(avg_c_g_before, p_intersect))] +
                                                             [np.array(to_vector(p_intersect, avg_c_g_after))]))
                        # we didn't find a proper line
                        if c_tangent_vec[0] == c_tangent_vec[1] == 0.:
                            continue
                    elif len(c_g_before) != 0:
                        # we use just the c_g_before points
                        avg_c_g_before = functools.reduce((lambda x, y: x + y), c_g_before) / len(c_g_before)
                        c_tangent_vec = to_point(unit_vector([np.array(to_vector(p_intersect, avg_c_g_before))]))
                    elif len(c_g_after) != 0:
                        # we use just the c_g_after points
                        avg_c_g_after = functools.reduce((lambda x, y: x + y), c_g_after) / len(c_g_after)
                        c_tangent_vec = to_point(unit_vector([np.array(to_vector(avg_c_g_after, p_intersect))]))
                    else:
                        continue

                    # Calculate the next_contour tangent vector at n_intersection_idx
                    n_g_before = next_contour[max(0, n_intesection_idx - gradient_points):n_intesection_idx]
                    n_g_after = next_contour[n_intesection_idx+1:n_intesection_idx+1+gradient_points]
                    n_g_points_considered = min(len(n_g_before), len(n_g_after))

                    if n_g_points_considered != 0:
                        n_g_before = n_g_before[-n_g_points_considered:]
                        n_g_after = n_g_after[:n_g_points_considered]

                        avg_n_g_before = functools.reduce((lambda x, y: x + y), n_g_before) / n_g_points_considered
                        avg_n_g_after = functools.reduce((lambda x, y: x + y), n_g_after) / n_g_points_considered

                        n_tangent_vec = to_point(unit_vector([np.array(to_vector(avg_n_g_before, p_intersect))] +
                                                             [np.array(to_vector(p_intersect, avg_n_g_after))]))
                        # we didn't find a proper line
                        if n_tangent_vec[0] == n_tangent_vec[1] == 0.:
                            continue
                    elif len(n_g_before) != 0:
                        # we use just the n_g_before points
                        avg_n_g_before = functools.reduce((lambda x, y: x + y), n_g_before) / len(n_g_before)
                        n_tangent_vec = to_point(unit_vector([np.array(to_vector(p_intersect, avg_n_g_before))]))
                    elif len(n_g_after) != 0:
                        # we use just the n_g_after points
                        avg_n_g_after = functools.reduce((lambda x, y: x + y), n_g_after) / len(n_g_after)
                        n_tangent_vec = to_point(unit_vector([np.array(to_vector(avg_n_g_after, p_intersect))]))
                    else:
                        continue

                    if debug_draw:
                        c_pt1 = (int(p_intersect[0] - c_tangent_vec[0] * 100), int(p_intersect[1] - c_tangent_vec[1] * 100))
                        c_pt2 = (int(p_intersect[0] + c_tangent_vec[0] * 100), int(p_intersect[1] + c_tangent_vec[1] * 100))
                        intersection = cv2.line(intersection, c_pt1, c_pt2, (255, 255, 0), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, c_pt2, (255, 255, 0), cv2.MARKER_DIAMOND)

                        n_pt1 = (int(p_intersect[0] - n_tangent_vec[0] * 100), int(p_intersect[1] - n_tangent_vec[1] * 100))
                        n_pt2 = (int(p_intersect[0] + n_tangent_vec[0] * 100), int(p_intersect[1] + n_tangent_vec[1] * 100))
                        intersection = cv2.line(intersection, n_pt1, n_pt2, (0, 255, 255), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, n_pt2, (0, 255, 255), cv2.MARKER_DIAMOND)
                        cv2.imshow('clean and connect contours - intersection', intersection)
                        cv2.waitKey(0)

                    angle = angle_between(c_tangent_vec, n_tangent_vec)
                    # Compare if the angles are within acceptable range to join the contour
                    if (c_intersect_beginning != n_intersect_beginning and angle <= max_angle) or (c_intersect_beginning == n_intersect_beginning and angle >= 180 - max_angle):
                        # Is the intersection at the beginning of both contours?
                        if c_intersect_beginning and n_intersect_beginning:
                            logger.debug("Intersection: beginning with beginning")
                            # Add the reversed next_contour to the beginning of contour
                            new_contour = np.concatenate((
                                # new_contour without the extra part, reversed and excl. the intersection point
                                next_contour[:n_intesection_idx:-1],
                                # contour without the extra part incl. the intersection point
                                contour[c_intesection_idx:]
                            ))
                        # Is the intersection at the end of both contours?
                        elif not c_intersect_beginning and not n_intersect_beginning:
                            logger.debug("Intersection: end with end")
                            # Add the reversed next_contour to the end of contour
                            new_contour = np.concatenate((
                                # contour without the extra part excl. the intersection point
                                contour[:c_intesection_idx],
                                # new_contour without the extra part, reversed and incl. the intersection point
                                next_contour[n_intesection_idx::-1]
                            ))
                        # Is the intersection at the beginning of contour and at the end of next_contour?
                        elif c_intersect_beginning and not n_intersect_beginning:
                            logger.debug("Intersection: beginning with end")
                            # Add next_contour to the beginning of contour
                            new_contour = np.concatenate((
                                # new_contour without the extra part and excl. the intersection point
                                next_contour[:n_intesection_idx],
                                # contour without the extra part incl. the intersection point
                                contour[c_intesection_idx:]
                            ))
                        # Is the intersection at the end of contour and at the beginning of next_contour?
                        elif not c_intersect_beginning and n_intersect_beginning:
                            logger.debug("Intersection: end with beginning")
                            # Add next_contour to the end of contour
                            new_contour = np.concatenate((
                                # contour without the extra part excl. the intersection point
                                contour[:c_intesection_idx],
                                # new_contour without the extra part and incl. the intersection point
                                next_contour[n_intesection_idx:]
                            ))
                    else:
                        continue

                    if debug_draw:
                        # Draw new contour in white
                        intersection = cv2.polylines(blank.copy(), [new_contour], False, (255, 255, 255), 1)
                        # Draw the end
                        intersection = cv2.drawMarker(intersection, to_point(new_contour[-1]), (255, 255, 255), cv2.MARKER_DIAMOND)
                        cv2.imshow('clean and connect contours - intersection', intersection)
                        cv2.waitKey(0)

                    contour_connected = True
                    # We have a joined new contour, update the list with it
                    sorted_contours[i] = new_contour
                    # Remove the joined line
                    del sorted_contours[j]
                    break

                if contour_connected:
                    break

            if contour_connected:
                break
            j += 1

        if not contour_connected:
            i += 1

        if debug_draw:
            if contour_connected:
                for p in range(len(sorted_contours[i]) - 1):
                    pt1 = to_point(sorted_contours[i][p])
                    pt2 = to_point(sorted_contours[i][p+1])
                    blank = cv2.line(blank, pt1, pt2, (0, 127, 255), 1)
                    cv2.imshow('clean and connect contours', blank)
                    cv2.waitKey(1)
            if i < len(sorted_contours):
                blank = cv2.polylines(blank, [sorted_contours[i]], False, (0, 255, 0), 1)

    return sorted_contours


def merge_similar_contours(img, contours, min_intersections=20, debug_draw=False):
    logger.debug(merge_similar_contours.__name__)
    logger.debug("number of contours: %d", len(contours))

    contours = sorted(contours, key=lambda x: len(x), reverse=True)

    if debug_draw:
        # Create a blank of same size as img supporting color
        blank = np.zeros(shape=(img.shape[0], img.shape[1], 3), dtype=img.dtype)
        cv2.imshow('merge similar contours', blank)
        cv2.waitKey(0)

    black = np.zeros_like(img)
    i = 0
    while i < len(contours):
        contours_merged = False
        contour = contours[i]

        if debug_draw:
            temp = cv2.polylines(blank.copy(), [contour], False, (0, 255, 0), 1)
            cv2.imshow('merge similar contours', temp)
            cv2.waitKey(0)

        j = i + 1
        while j < len(contours):
            next_contour = contours[j]

            # Test intersection by drawing; use 2 pixel width to make almost matches count as well
            intersections = cv2.polylines(black.copy(), [contour], False, (255, 255, 255), 2) & \
                cv2.polylines(black.copy(), [next_contour], False, (255, 255, 255), 2)
            number_intersections = cv2.countNonZero(intersections)

            if number_intersections >= min_intersections:
                # Now we merge the lines
                # @Note @Incomplete dummy implementation just keeping the longest contour
                # @TODO Actually merge them
                new_contour = contour if len(next_contour) < len(contour) else next_contour

                contours_merged = True
                # We have a joined new contour, update the list with it
                contours[i] = new_contour
                # Remove the joined line
                del contours[j]
                break

            if contours_merged:
                break
            j += 1

        if not contours_merged:
            i += 1

    return contours


def contours(img, *contours_fixers, initial_contours=None, contour_thickness=cv2.FILLED, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE, line_type=cv2.LINE_4, fill=False, debug_draw=False):
    if initial_contours is None:
        contours, _ = cv2.findContours(img.copy(), mode, method)
    else:
        contours = initial_contours

    blank = np.zeros_like(img)
    if debug_draw:
        draw_contours(img, contours, debug_draw=True)
    if contours_fixers is not None:
        for f in contours_fixers:
            contours, _ = f(img, contours)

            if debug_draw:
                draw_contours(img, contours, debug_draw=True)

    if fill:
        blank = cv2.drawContours(blank, contours, -1, (255, 255, 255), contour_thickness, line_type)
    else:
        blank = cv2.polylines(blank, contours, False, (255, 255, 255), 2 if contour_thickness == cv2.FILLED else contour_thickness, line_type)

    if debug_draw:
        cv2.imshow("drawn contours", blank)

    return blank, contours
